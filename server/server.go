package rpcserver

import (
	"context"
	"log"
	"net"

	pb_events "github.com/nickkhall/tracker-rpc/proto/events"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

// TrackerRPCServer server
type TrackerRPCServer struct {
	pb_events.UnimplementedEventsServiceServer
}

// GetEvents -> Gets all calendar events
func (s *TrackerRPCServer) GetEvents(ctx context.Context, req *pb_events.GetEventsRequest) (*pb_events.GetEventsResponse, error) {
	// get events
	return &pb_events.GetEventsResponse{
		Events: nil,
	}, nil
}

// GetEvent -> Gets a single event
func (s *TrackerRPCServer) GetEvent(ctx context.Context, req *pb_events.GetEventRequest) (*pb_events.GetEventResponse, error) {
	message := "Ok"
	// get events
	return &pb_events.GetEventResponse{
		Success: true,
		Message: message,
		Event:   nil,
	}, nil
}

// CreateEvent -> Creates an event
func (s *TrackerRPCServer) CreateEvent(ctx context.Context, req *pb_events.CreateEventRequest) (*pb_events.CreateEventResponse, error) {
	message := "Ok"
	// get events
	return &pb_events.CreateEventResponse{
		Success: true,
		Message: message,
		Event:   nil,
	}, nil
}

// UpdateEvent -> Updates an event
func (s *TrackerRPCServer) UpdateEvent(ctx context.Context, req *pb_events.UpdateEventRequest) (*pb_events.UpdateEventResponse, error) {
	message := "Ok"
	// get events
	return &pb_events.UpdateEventResponse{
		Success: true,
		Message: message,
		Event:   nil,
	}, nil
}

// RemoveEvent -> Removes an event
func (s *TrackerRPCServer) RemoveEvent(ctx context.Context, req *pb_events.RemoveEventRequest) (*pb_events.RemoveEventResponse, error) {
	message := "Ok"
	// get events
	return &pb_events.RemoveEventResponse{
		Success: true,
		Message: message,
	}, nil
}

// Execute -> Starts the Tracker RPC Server
func Execute() {
	addr := viper.GetString("addr")
	port := viper.GetString("port")

	listener, err := net.Listen("tcp", net.JoinHostPort(addr, port))
	if err != nil {
		log.Fatalf("Failed to listen on port %s: %v", port, err)
	}

	s := grpc.NewServer()

	pb_events.RegisterEventsServiceServer(s, &TrackerRPCServer{})

	err = s.Serve(listener)
	if err != nil {
		log.Fatalf("Failed to start rpc server: %v", err)
	}
}
