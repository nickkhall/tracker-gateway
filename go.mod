module github.com/nickkhall/tracker-rpc

go 1.16

require (
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/spf13/cobra v1.2.1 // indirect
	github.com/spf13/viper v1.9.0 // indirect
	google.golang.org/grpc v1.40.0 // indirect
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
