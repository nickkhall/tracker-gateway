package database

import "database/sql"

// Exportable database connection
var (
	DBConnection *sql.DB
)
