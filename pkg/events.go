package events

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	database "github.com/nickkhall/tracker-rpc/database"
	pb_events "github.com/nickkhall/tracker-rpc/proto/events"
)

const (
	host     = ""
	port     = 5432
	user     = ""
	password = ""
	dbname   = ""
)

// GetEvents -> Gets all calendar events
func GetEvents() (*pb_events.EventsResponse, error) {
	var err error
	sqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	database.DBConnection, err = sql.Open("postgres", sqlInfo)
	if err != nil {
		fmt.Println("Failed to connect to Postgres in GetEvents")
		log.Fatal(err)
	}

	eventRows, err := database.DBConnection.Query("SELECT * FROM events")

	if err != nil {
		fmt.Println("Failed to query Postgres for all events")
		log.Fatal(err)
	}

	events := []*pb_events.Event{}

	defer eventRows.Close()

	for eventRows.Next() {
		var (
			id          string
			time        int64
			title       string
			description string
			day         int32
			month       int32
			year        int32
		)

		err = eventRows.Scan(&id, &time, &title, &description, &day, &month, &year)
		if err != nil {
			log.Fatal(err)
		}

		event := &pb_events.Event{
			Id:          id,
			Time:        time,
			Title:       title,
			Description: description,
			Day:         day,
			Month:       month,
			Year:        year,
		}

		events = append(events, event)
	}

	eventsResponse := &pb_events.EventsResponse{
		Events: events,
	}

	return eventsResponse, nil
}
