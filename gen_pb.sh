#!/usr/bin/env bash

# Generate Events Protobufs
rm -rf proto/events/*pb*
protoc --go-grpc_out=. proto/events/events.proto
protoc --go_out=. proto/events/events.proto


